<?php
class Dashboard extends MY_Controller {
  function __construct() {
    parent::__construct();
  }

  private function _login($uname, $passwd) {
    $res = array('message'=>'', 'data'=>null);
    $ruser = $this->db
    ->where(COL_USERID, $uname)
    ->or_where(COL_EMAIL, $uname)
    ->get(TBL__USERS)
    ->row_array();
    if(empty($ruser)) $res['message'] = LOGIN_NOTFOUND;
    else if($ruser[COL_PASSWORD] != md5($passwd)) $res['message'] = LOGIN_WRONGPASSWORD;
    else if($ruser[COL_ISSUSPEND]) $res['message'] = LOGIN_SUSPENDED;
    else {
      $res['message'] = LOGIN_SUCCESS;
      $res['data'] = $ruser;
    }
    return $res;
  }

  public function index() {
    if(!IsLogin()) {
      redirect('admin/dashboard/login');
    }

    $data['title'] = 'Dashboard';
    $this->template->load('main', 'admin/dashboard/index', $data);
  }

  public function logout(){
    UnsetLoginSession();
    redirect(site_url());
  }

  public function login() {
    UnsetLoginSession();
    if(IsLogin()) {
      redirect('admin/dashboard');
    }
    $data['title'] = "Login";
    $rules = array(
      array(
          'field' => COL_USERID,
          'label' => COL_USERID,
          'rules' => 'required'
      ),
      array(
          'field' => 'Password',
          'label' => 'Password',
          'rules' => 'required'
      )
    );
    $this->form_validation->set_rules($rules);
    if($this->form_validation->run()){
      $username = $this->input->post(COL_USERID);
      $password = $this->input->post(COL_PASSWORD);

      $login = $this->_login($username, $password);
      if($login['message'] != LOGIN_SUCCESS) {
        $data['err'] = $login['message'];
        $this->load->view('admin/dashboard/_login', $data);
      } else {
        $this->db->where(COL_USERID, $username);
        $this->db->update(TBL__USERS, array(COL_LASTLOGIN=>date('Y-m-d H:i:s')));
        SetLoginSession($login['data']);
        redirect('admin/dashboard');
      }

    } else {
      $this->load->view('admin/dashboard/_login', $data);
    }
  }

  function _404(){
    echo '404';
  }
}
?>
