<?php
class User extends MY_Controller {
  public $usr;
  public $uploadconf = array(
    'upload_path' => MY_UPLOADPATH,
    'allowed_types' => UPLOAD_ALLOWEDTYPES,
    'max_size' => 5000,
    'max_width' => 2048,
    'max_height' => 2048,
    'overwrite' => TRUE
  );

  function __construct() {
    parent::__construct();
    if(IsLogin()) {
      $this->usr = GetLoggedUser();
    }
    $this->load->library('upload', $this->uploadconf);
  }

  private function _upload($postname, $filename) {
    $res = array('status'=>'OK', 'filename'=>'');
    $dataupload = null;

    $this->upload->initialize(array_merge($this->uploadconf, array('file_name'=>$filename)));
    if(!$this->upload->do_upload($postname)){
      $err = $this->upload->display_errors();
      $res['status'] = strip_tags($err);
    }

    $dataupload = $this->upload->data();
    if(!empty($dataupload) && $dataupload['file_name']) {
      $res['filename'] = $dataupload['file_name'];
    }

    return $res;
  }

  public function load($from=null, $to=null, $limit=null) {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $role = $_POST['role'];

    $ruser = GetLoggedUser();
    $orderdef = array(COL_NAMA=>'asc');
    $orderables = array(null,COL_NAMA,null,COL_EMAIL,COL_NIK,COL_TANGGALREGISTRASI,COL_LASTLOGIN);
    $cols = array(COL_NAMA,COL_ISSUSPEND,COL_EMAIL,COL_NIK,COL_TANGGALREGISTRASI,COL_LASTLOGIN);
    $numrows = $this->db->where(COL_ROLEID, $role)->get(TBL__USERS)->num_rows();

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }
    $q = $this->db
    ->where(COL_ROLEID, $role)
    ->get_compiled_select(TBL__USERS, FALSE);

    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start")->result_array();
    $data = [];

    foreach($rec as $r) {
      $data[] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="'.$r[COL_USERID].'" />',
        anchor('admin/user/edit/'.GetEncryption($r[COL_USERID]),$r[COL_NAMA]),
        $r[COL_ISSUSPEND] ? '<small class="badge pull-left badge-danger">SUSPENDED</small>' : '<small class="badge pull-left badge-success">ACTIVE</small>',
        $r[COL_EMAIL],
        $r[COL_NIK],
        (!empty($r[COL_TANGGALREGISTRASI])?date('d/m/Y', strtotime($r[COL_TANGGALREGISTRASI])):"-"),
        (!empty($r[COL_LASTLOGIN])?date('d/m/Y H:i:s', strtotime($r[COL_LASTLOGIN])):"-")
      );
    }


    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $numrows,
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function index($role=0) {
    if(!IsLogin()) {
      redirect('admin/dashboard/login');
    }

    $data['role'] = $role;
    $data['title'] = 'Akun Pengguna';
    $this->template->load('main', 'admin/user/index', $data);
  }

  public function add($role='') {
    $data['title'] = "Form Pengguna";
    $data['role'] = $role;

    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => COL_EMAIL,
          'label' => COL_EMAIL,
          'rules' => 'required|valid_email|is_unique[_users.Email]',
          'errors' => array('is_unique' => 'Email sudah digunakan.')
        ),
        array(
          'field' => COL_NIK,
          'label' => COL_NIK,
          'rules' => 'is_unique[_users.NIK]',
          'errors' => array('is_unique' => 'NIK sudah terdaftar.')
        ),
        array(
          'field' => COL_PASSWORD,
          'label' => COL_PASSWORD,
          'rules' => 'required|min_length[5]',
          'errors' => array('min_length' => 'Password minimal terdiri dari 5 karakter.')
        ),
        array(
          'field' => 'ConfirmPassword',
          'label' => 'Repeat ConfirmPassword',
          'rules' => 'required|matches[Password]',
          'errors' => array('matches' => 'Kolom Ulangi Password wajib sama dengan Password.')
        )
      ));

      if(!$this->form_validation->run()) {
        $err = validation_errors();
        ShowJsonError($err);
        return;
      }

      if(empty($role)) {
        $role = $this->input->post(COL_ROLEID);
      }
      $email = $this->input->post(COL_EMAIL);
      $userdata = array(
        COL_PASSWORD => md5($this->input->post(COL_PASSWORD)),
        COL_ROLEID => $role,
        COL_EMAIL => $email,
        COL_NAMA => $this->input->post(COL_NAMA),
        COL_NIK => $this->input->post(COL_NIK),
        COL_NOTELP => $this->input->post(COL_NOTELP),
        COL_ALAMAT => $this->input->post(COL_ALAMAT),
        COL_TANGGALLAHIR => date('Y-m-d', strtotime($this->input->post(COL_TANGGALLAHIR))),
        COL_TANGGALREGISTRASI => date('Y-m-d')
      );

      if(!empty($_FILES[COL_FOTO]["name"])) {
        $uploadname = explode(".", $_FILES[COL_FOTO]["name"]);
        $fname = 'foto-'.$userdata[COL_NIK].'-'.date('YmdHis');
        $fname = strtolower($fname).".".end($uploadname);
        $uploadFoto = $this->_upload(COL_FOTO, $fname);
        if($uploadFoto['status'] != 'OK') {
          ShowJsonError($uploadFoto['status']);
          return;
        }

        $userdata[COL_FOTO] = $uploadFoto['filename'];
      }

      if(!empty($_FILES[COL_FOTOKTP]["name"])) {
        $uploadname = explode(".", $_FILES[COL_FOTOKTP]["name"]);
        $fname = 'ktp-'.$userdata[COL_NIK].'-'.date('YmdHis');
        $fname = strtolower($fname).".".end($uploadname);
        $uploadFoto = $this->_upload(COL_FOTOKTP, $fname);
        if($uploadFoto['status'] != 'OK') {
          ShowJsonError($uploadFoto['status']);
          return;
        }

        $userdata[COL_FOTOKTP] = $uploadFoto['filename'];
      }

      $res = true;
      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL__USERS, $userdata);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        if($role == ROLEMERCHANT) {
          $iduser = $this->db->insert_id();
          $business = array(
            COL_IDPEMILIK => $iduser,
            COL_NAMA => $this->input->post("BusinessName"),
            COL_ALAMAT => $this->input->post("BusinessAddress"),
            COL_LAT => $this->input->post(COL_LAT),
            COL_LONG => $this->input->post(COL_LONG),
            COL_ISHALAL => $this->input->post(COL_ISHALAL) == 'on',
            COL_ISBUKA => $this->input->post(COL_ISBUKA) == 'on',
            COL_ISAKTIF => $this->input->post(COL_ISAKTIF) == 'on',
            COL_CREATEDON => date('Y-m-d H:i:s'),
            COL_CREATEDBY => $this->usr[COL_EMAIL]
          );

          $res = $this->db->insert(TBL_M_RESTO, $business);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }

          $idresto = $this->db->insert_id();
          $arrSchedule = array();
          $postSchedule = $this->input->post("JadwalBuka");
          if(!empty($postSchedule)) {
            $postSchedule = json_decode(urldecode($postSchedule));
            foreach($postSchedule as $s) {
              $arrSchedule[] = array(
                COL_IDRESTO=>$idresto,
                COL_IDHARI=>$s->Day,
                COL_JAMBUKA=>$s->TimeFrom,
                COL_JAMTUTUP=>$s->TimeTo
              );
            }

            $res = $this->db->insert_batch(TBL_M_RESTOJADWAL, $arrSchedule);
            if(!$res) {
              throw new Exception('Error: '.$this->db->error());
            }
          }
        } else if($role == ROLEKURIR) {
          $iduser = $this->db->insert_id();
          $kurir = array(
            COL_IDPENGGUNA => $iduser,
            COL_NAMA => $this->input->post(COL_NAMA),
            COL_NOSIM => $this->input->post(COL_NOSIM),
            COL_NOPLAT => $this->input->post(COL_NOPLAT),
            COL_ISAKTIF => $this->input->post("isDriverAktif") == 'on',
            COL_CREATEDON => date('Y-m-d H:i:s'),
            COL_CREATEDBY => $this->usr[COL_EMAIL]
          );

          $res = $this->db->insert(TBL_M_DRIVER, $kurir);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil', array('redirect'=>site_url('admin/user/index/'.$role)));
        return;
      } catch (Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }
    }

    $this->template->load('main', 'admin/user/form', $data);
  }

  public function edit($id) {
    $id = GetDecryption($id);
    $rdata = $this->db
    ->where(TBL__USERS.".".COL_USERID, $id)
    ->get(TBL__USERS)->row_array();

    if(empty($rdata)) {
      show_error('Data tidak ditemukan.');
      return;
    }

    $data['title'] = "Form Pengguna";
    $data['edit'] = true;
    $data['role'] = $rdata[COL_ROLEID];
    $data['data'] = $rdata;

    if(!empty($_POST)) {
      $userinfo = array(
        COL_NAMA => $this->input->post(COL_NAMA),
        COL_NIK => $this->input->post(COL_NIK),
        COL_NOTELP => $this->input->post(COL_NOTELP),
        COL_ALAMAT => $this->input->post(COL_ALAMAT),
        COL_TANGGALLAHIR => date('Y-m-d', strtotime($this->input->post(COL_TANGGALLAHIR))),
        COL_TANGGALREGISTRASI => date('Y-m-d')
      );

      if(!empty($_POST[COL_PASSWORD]) && !empty($_POST['ConfirmPassword'])) {
        if($_POST[COL_PASSWORD] == $_POST['ConfirmPassword']) {
          $userinfo[COL_PASSWORD] = md5($this->input->post(COL_PASSWORD));
        }
      }

      if(!empty($_FILES[COL_FOTO]["name"])) {
        $uploadname = explode(".", $_FILES[COL_FOTO]["name"]);
        $fname = 'foto-'.$userinfo[COL_NIK].'-'.date('YmdHis');
        $fname = strtolower($fname).".".end($uploadname);
        $uploadFoto = $this->_upload(COL_FOTO, $fname);
        if($uploadFoto['status'] != 'OK') {
          ShowJsonError($uploadFoto['status']);
          return;
        }

        $userinfo[COL_FOTO] = $uploadFoto['filename'];
        if(!empty($rdata[COL_FOTO]) && file_exists(MY_UPLOADPATH.$rdata[COL_FOTO])) {
          unlink(MY_UPLOADPATH.$rdata[COL_FOTO]);
        }
      }

      if(!empty($_FILES[COL_FOTOKTP]["name"])) {
        $uploadname = explode(".", $_FILES[COL_FOTOKTP]["name"]);
        $fname = 'ktp-'.$userinfo[COL_NIK].'-'.date('YmdHis');
        $fname = strtolower($fname).".".end($uploadname);
        $uploadFoto = $this->_upload(COL_FOTOKTP, $fname);
        if($uploadFoto['status'] != 'OK') {
          ShowJsonError($uploadFoto['status']);
          return;
        }

        $userinfo[COL_FOTOKTP] = $uploadFoto['filename'];
        if(!empty($rdata[COL_FOTOKTP]) && file_exists(MY_UPLOADPATH.$rdata[COL_FOTOKTP])) {
          unlink(MY_UPLOADPATH.$rdata[COL_FOTOKTP]);
        }
      }

      $res = true;
      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_USERID, $id)->update(TBL__USERS, $userinfo);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        if($rdata[COL_ROLEID] == ROLEMERCHANT) {
          $idresto = $_POST[COL_IDRESTO];
          if(!empty($idresto)) {
            $business = array(
              COL_NAMA => $this->input->post("BusinessName"),
              COL_ALAMAT => $this->input->post("BusinessAddress"),
              COL_LAT => $this->input->post(COL_LAT),
              COL_LONG => $this->input->post(COL_LONG),
              COL_ISHALAL => $this->input->post(COL_ISHALAL) == 'on',
              COL_ISBUKA => $this->input->post(COL_ISBUKA) == 'on',
              COL_ISAKTIF => $this->input->post(COL_ISAKTIF) == 'on'
            );
            $res = $this->db->where(COL_IDRESTO, $idresto)->update(TBL_M_RESTO, $business);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception('Error: '.$err['message']);
            }

            $arrSchedule = array();
            $postSchedule = $this->input->post("JadwalBuka");
            if(!empty($postSchedule)) {
              $postSchedule = json_decode(urldecode($postSchedule));
              foreach($postSchedule as $s) {
                $arrSchedule[] = array(
                  COL_IDRESTO=>$idresto,
                  COL_IDHARI=>$s->Day,
                  COL_JAMBUKA=>$s->TimeFrom,
                  COL_JAMTUTUP=>$s->TimeTo
                );
              }

              $res = $this->db->where(COL_IDRESTO, $idresto)->delete(TBL_M_RESTOJADWAL);
              $res = $this->db->insert_batch(TBL_M_RESTOJADWAL, $arrSchedule);
              if(!$res) {
                throw new Exception('Error: '.$this->db->error());
              }
            }
          }
        } else if($rdata[COL_ROLEID] == ROLEKURIR) {
          $iddriver = $_POST[COL_IDDRIVER];
          if(!empty($iddriver)) {
            $kurir = array(
              COL_IDPENGGUNA => $iduser,
              COL_NAMA => $this->input->post(COL_NAMA),
              COL_NOSIM => $this->input->post(COL_NOSIM),
              COL_NOPLAT => $this->input->post(COL_NOPLAT),
              COL_ISAKTIF => $this->input->post("isDriverAktif") == 'on'
            );
            $res = $this->db->where(COL_IDDRIVER, $iddriver)->update(TBL_M_DRIVER, $kurir);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception('Error: '.$err['message']);
            }
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil', array('redirect'=>site_url('admin/user/index/'.$rdata[COL_ROLEID])));
        return;
      } catch (Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }
    }
    $this->template->load('main', 'admin/user/form', $data);
  }

  public function delete() {
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
      $rdata = $this->db
      ->where(COL_USERID, $datum)
      ->get(TBL__USERS)
      ->row_array();

      $res = $this->db->where(COL_USERID, $datum)->delete(TBL__USERS);
      if($res) {
        $deleted++;
        if(!empty($rdata) && file_exists(MY_UPLOADPATH.$rdata[COL_FOTO])) {
          unlink(MY_UPLOADPATH.$rdata[COL_FOTO]);
        }
        if(!empty($rdata) && file_exists(MY_UPLOADPATH.$rdata[COL_FOTOKTP])) {
          unlink(MY_UPLOADPATH.$rdata[COL_FOTOKTP]);
        }
      }
    }
    if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
    }else{
        ShowJsonError("Tidak ada data dihapus");
    }
  }

  public function activate($opt = 0) {
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
      if($opt == 0 || $opt == 1) {
        if($this->db->where(COL_USERID, $datum)->update(TBL__USERS, array(COL_ISSUSPEND=>$opt))) {
          $deleted++;
        }
      }
      else {
        if($this->db->where(COL_USERID, $datum)->update(TBL__USERS, array(COL_PASSWORD=>MD5('123456')))) {
          $deleted++;
        }
      }
    }
    if($deleted){
      ShowJsonSuccess($deleted." data diubah");
    }else{
      ShowJsonError("Tidak ada data yang diubah");
    }
  }
}
?>
