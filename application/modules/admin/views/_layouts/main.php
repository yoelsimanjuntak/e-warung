<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?></title>
  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/modernizr/modernizr.js"></script>
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/datatable/media/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/dataTables.bootstrap.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


  <!-- datatable reorder _ buttons ext + resp + print -->
  <!--<script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/ColReorderWithResize.js"></script>-->
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
  <link href="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?=base_url()?>assets/datatable/ext/responsive/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/jszip/jszip.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/pdfmake.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/vfs_fonts.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/responsive/js/dataTables.responsive.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.html5.min.js"></script>

  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/css/jquery.timepicker.min.css">
  <link rel="icon" type="image/png" href=<?=MY_IMAGEURL.$this->setting_web_logo?>>

  <style>
  .no-js #loader { display: none;  }
  .js #loader { display: block; position: absolute; left: 100px; top: 0; }
  .se-pre-con {
      position: fixed;
      left: 0px;
      top: 0px;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background: url(<?=base_url()?>assets/preloader/images/<?=$this->setting_web_preloader?>) center no-repeat #fff;
  }

  @media (max-width: 767px) {
      .sidebar-toggle {
          font-size: 3vw !important;
      }
  }

  .form-group .control-label {
      text-align: right;
      line-height: 2;
  }

  .toast-message p {
    margin: 0px !important
  }
  </style>
  <script>
  $(window).load(function() {
      // Animate loader off screen
      $(".se-pre-con").fadeOut("slow");
  });
  </script>
</head>
<body class="hold-transition sidebar-mini layout-navbar-fixed">
<div class="se-pre-con"></div>
<div class="wrapper">
  <?php
  $ruser = GetLoggedUser();
  $displayname = $ruser ? $ruser[COL_NAMA] : "Guest";
  $displaypicture = MY_IMAGEURL.'user.jpg';
  if($ruser) {
      $displaypicture = $ruser[COL_FOTO] ? MY_UPLOADURL.$ruser[COL_FOTO] : MY_IMAGEURL.'user.jpg';
  }
  ?>
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="z-index: 1040 !important">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#">
          <i class="fas fa-bars"></i>
          <!--<?=$this->setting_web_desc?> <sup>ver <?=$this->setting_web_version?></sup>-->
        </a>
      </li>
    </ul>
  </nav>

  <aside class="main-sidebar sidebar-light-olive elevation-2">
    <a href="<?=site_url()?>" class="brand-link" style="background-color: #fff; padding-left: .5rem">
      <img src="<?=MY_IMAGEURL.$this->setting_web_logo?>" alt="Logo" class="brand-image ml-0" style="padding-left: .8rem;">
      <span class="brand-text font-weight-light"><?=$this->setting_web_name?></span>
    </a>
    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?=$displaypicture?>" class="img-circle elevation-2" alt="Your Profile Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?=$displayname?></a>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="<?=site_url('admin/dashboard')?>" class="nav-link">
              <i class="nav-icon fa fa-home"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <?php
          if($ruser[COL_ROLEID] == ROLEADMIN) {
              ?>
              <li class="nav-item">
                <a href="<?=site_url('admin/user/index/'.ROLECUSTOMER)?>" class="nav-link">
                  <i class="nav-icon far fa-circle"></i>
                  <p>Konsumen</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=site_url('admin/user/index/'.ROLEMERCHANT)?>" class="nav-link">
                  <i class="nav-icon far fa-circle"></i>
                  <p>Merchant</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=site_url('admin/user/index/'.ROLEKURIR)?>" class="nav-link">
                  <i class="nav-icon far fa-circle"></i>
                  <p>Kurir</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=site_url('admin/business')?>" class="nav-link">
                  <i class="nav-icon far fa-circle"></i>
                  <p>Restoran</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="nav-icon far fa-circle"></i>
                  <p>Pesanan</p>
                </a>
              </li>
              <?php
          }
          ?>
          <li class="nav-header">ACCOUNT</li>
          <li class="nav-item">
            <a href="<?=site_url('admin/dashboard/changepassword')?>" class="nav-link">
              <i class="nav-icon fa fa-key"></i>
              <p>Ubah Password</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?=site_url('admin/dashboard/logout')?>" class="nav-link">
              <i class="nav-icon fa fa-sign-out"></i>
              <p>Logout</p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>

  <div class="content-wrapper">
    <?=$content?>
  </div>

  <footer class="main-footer">
    <div class="float-right d-none d-sm-inline">
      <b>Version</b> <?=$this->setting_web_version?>
    </div>
    <strong>Copyright &copy; <?=date("Y")?> <?=$this->setting_web_name?></strong>. Strongly developed by <b>Partopi Tao</b>.
  </footer>
</div>

<div class="modal fade" id="confirmDialog" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirm Dialog</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary btn-ok">OK</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade modal-danger" id="alertDialog" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Alert!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade modal-default" id="promptDialog" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
        <h4 class="modal-title">Prompt</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary btn-flat btn-ok">OK</button>
      </div>
    </div>
  </div>
</div>
</body>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/dist/js/adminlte.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/raphael/raphael.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jquery-mapael/maps/world_countries.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/chart.js.old/Chart.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/fastclick/fastclick.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/sparklines/sparkline.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/iCheck/icheck.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/js/select2.full.min.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-select.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/template/js/function.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.form.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="<?=base_url()?>assets/js/ckeditor/ckeditor.js"></script>
<script src="<?=base_url()?>assets/js/moment.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="<?=base_url()?>assets/js/jspdf/jspdf.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jspdf/libs/png_support/zlib.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jspdf/libs/png_support/png.js"></script>
<script src="<?=base_url()?>assets/js/jspdf/plugins/from_html.js"></script>
<script src="<?=base_url()?>assets/js/jspdf/plugins/addimage.js"></script>
<script src="<?=base_url()?>assets/js/jspdf/plugins/png_support.js"></script>
<script src="<?=base_url()?>assets/js/jspdf/plugins/split_text_to_size.js"></script>
<script src="<?=base_url()?>assets/js/jspdf/plugins/standard_fonts_metrics.js"></script>
<script src="<?=base_url()?>assets/js/jspdf/plugins/filesaver.js"></script>
<script src="<?=base_url()?>assets/js/html2canvas.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.number.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/inputmask/jquery.inputmask.bundle.js"></script>

<!--<script type="text/javascript" src="--><?//=base_url()?><!--assets/tbs/js/plugins/datatables/jquery.dataTables.js"></script>-->
<!--<script type="text/javascript" src="--><?//=base_url()?><!--assets/tbs/js/plugins/datatables/dataTables.bootstrap.js"></script>-->
<!--<script type="text/javascript" src="--><?//=base_url()?><!--assets/tbs/js/plugins/datatables/ColReorderWithResize.js"></script>-->
<!--<script type="text/javascript" src="--><?//=base_url()?><!--assets/js/bootstrap-multiselect.js"></script>-->

<script src="<?=base_url()?>assets/js/jquery.timepicker.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>
<script>
Date.prototype.setISO8601 = function (string) {
  var regexp = "([0-9]{4})(-([0-9]{2})(-([0-9]{2})" +
      "(T([0-9]{2}):([0-9]{2})(:([0-9]{2})(\.([0-9]+))?)?" +
      "(Z|(([-+])([0-9]{2}):([0-9]{2})))?)?)?)?";
  var d = string.match(new RegExp(regexp));

  var offset = 0;
  var date = new Date(d[1], 0, 1);

  if (d[3]) { date.setMonth(d[3] - 1); }
  if (d[5]) { date.setDate(d[5]); }
  if (d[7]) { date.setHours(d[7]); }
  if (d[8]) { date.setMinutes(d[8]); }
  if (d[10]) { date.setSeconds(d[10]); }
  if (d[12]) { date.setMilliseconds(Number("0." + d[12]) * 1000); }
  if (d[14]) {
      offset = (Number(d[16]) * 60) + Number(d[17]);
      offset *= ((d[15] == '-') ? 1 : -1);
  }

  offset -= date.getTimezoneOffset();
  time = (Number(date) + (offset * 60 * 1000));
  this.setTime(Number(time));
};

function DuaDigit(x){
  x = x.toString();
  var len = x.length;
  if(len < 2){
      return "0"+x;
  }else{
      return x;
  }
}

var mymodal;
function UseModal(){
  var modalcontent = '<div class="modal fade" id="generalModal" tabindex="-1" role="dialog" aria-hidden="true">'+
      '<div class="modal-dialog modal-lg">'+
      '<div class="modal-content">'+
      '<div class="modal-header">'+
      '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
      '<h4 class="modal-title" id="myModalLabel">Modal title</h4>'+
      '</div>'+
      '<div class="modal-body">'+
      '</div>'+
      '<div class="modal-footer">'+
      '<button type="button" class="btn btn-default modalCancel" data-dismiss="modal">Cancel</button>'+
      '<button type="button" class="btn btn-primary modalOK">OK</button>'+
      '</div>'+
      '</div>'+
      '</div>';
  if(!$('#generalModal').length){
      $(modalcontent).appendTo('body');
      mymodal = $('#generalModal');
  }
}

function LoadModal(url,data,cb){
  mymodal.find('.modal-body').empty().load(url,data,cb);
}

function ModalTitle(title){
  mymodal.find('#myModalLabel').empty().text(title);
}

function CloseModal(){
  mymodal.modal('hide');
}

function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp*1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  var time = DuaDigit(date)+' '+month+' '+year+' '+DuaDigit(hour)+':'+DuaDigit(min)+':'+DuaDigit(sec);
  return time;
}

$(document).ready(function(){
  $('[data-mask]').inputmask();
  $('.ui').button();
  $('a[href="<?=current_url()?>"]').addClass('active');
  $('.dropdown-menu textarea,.dropdown-menu input, .dropdown-menu label').click(function(e) {
    e.stopPropagation();
  });
  $(document).on('keypress','.angka',function(e){
    if((e.which <= 57 && e.which >= 48) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode==9 || e.which==43 || e.which==44 || e.which==45 || e.which==46 || e.keyCode==8){
      return true;
    }else{
      return false;
    }
  });
  $('input[type=file]').on('change',function(){
    var fileName = $(this).val();
    $(this).next('.custom-file-label').html(fileName);
  });
  $('.timepicker').timepicker({
    timeFormat: 'HH:mm',
    interval: 30,
    minTime: '7',
    maxTime: '23',
    defaultTime: '7',
    startTime: '07:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true,
    zindex: 9999999
  });
  /*$(document).on('blur','.uang',function(){
      $(this).val(desimal($(this).val(),0));
  }).on('focus','.uang',function(){
      $(this).val(toNum($(this).val()));
  });
  $(".uang").trigger("blur");*/

  var confirmDialog = $("#confirmDialog");
  var alertDialog = $("#alertDialog");
  confirmDialog.on("hidden.bs.modal", function(){
    $(".modal-body", confirmDialog).html("");
    $("button", confirmDialog).unbind();
  });
  alertDialog.on("hidden.bs.modal", function(){
    $(".modal-body", alertDialog).html("");
    $("button", alertDialog).unbind();
  });

  if($('.cekboxaction').length){
    $('.cekboxaction').click(function(){
      var a = $(this);
      var confirmDialog = $("#confirmDialog");
      var alertDialog = $("#alertDialog");

      confirmDialog.on("hidden.bs.modal", function(){
        $(".modal-body", confirmDialog).html("");
      });
      alertDialog.on("hidden.bs.modal", function(){
        $(".modal-body", alertDialog).html("");
      });

      if($('.cekbox:checked').length < 1){
        toastr.error('Tidak ada data dipilih');
        return false;
      }

      $(".modal-body", confirmDialog).html((a.data('confirm')||"Are you sure?"));
      confirmDialog.modal("show");
      $(".btn-ok", confirmDialog).click(function() {
        var thisBtn = $(this);
        thisBtn.html("Loading...").attr("disabled", true);
        $('#dataform').ajaxSubmit({
          dataType: 'json',
          url : a.attr('href'),
          success : function(data){
            if(data.error==0){
              toastr.success(data.success);
              window.location.reload();
            }else{
              toastr.error(data.error);
            }
          },
          complete: function(){
            thisBtn.html("OK").attr("disabled", false);
            confirmDialog.modal("hide");
          }
        });
      });
      return false;
    });
  }
  if($('.cekboxtarget').length){
    $('.cekboxtarget').click(function(){
      var a = $(this);
      var confirmDialog = $("#confirmDialog");
      var alertDialog = $("#alertDialog");

      if($('.cekbox:checked').length < 1){
        toastr.error('Tidak ada data dipilih');
        return false;
      }

      $(".modal-body", confirmDialog).html(a.attr("confirm").replace("{count}", $('.cekbox:checked').length));
      confirmDialog.modal("show");
      $(".btn-ok", confirmDialog).click(function() {
        $('#dataform').attr("action", a.attr('href')).attr("target", "_blank").submit();
        confirmDialog.modal("hide");
      });
      return false;
    });
  }

  $('a[href="<?=current_url()?>"]').addClass('active');
  $('a[href="<?=current_url()?>"]').closest('li.has-treeview').addClass('menu-open').children('.nav-link').addClass('active');
  //$('li.treeview.active').find('ul').eq(0).show();
  //$('li.treeview.active').find('.fa-angle-left').removeClass('fa-angle-left').addClass('fa-angle-down');

  $(".editor").wysihtml5();
  $("select").not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  //$("select").selectpicker();
  $('.datepicker').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1900,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
        format: 'Y-MM-DD'
    }
  });
  $('.datepicker').on('show.daterangepicker', function(ev, picker) {
    console.log("calendar is here");
  });

  /*$(".alert").fadeTo(2000, 500).slideUp(500, function(){
      $(".alert").slideUp(500);
  });*/
  $( ".alert-dismissible" ).fadeOut(3000, function() {
      // Animation complete.
  });
  //iCheck for checkbox and radio inputs
  /*$('input[type="checkbox"], input[type="radio"]').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
  });*/
  $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
  });

  $('.colorpick').colorpicker();
  $('.colorpick').on('colorpickerChange', function(event) {
      $('.colorpick .fa-square').css('color', event.color.toString());
  });

  $(".money").number(true, 2, '.', ',');
  $(".uang").number(true, 0, '.', ',');

  /*$.ajaxSetup({
      beforeSend: function(xhr) {
          xhr.setRequestHeader('HTTP_X_REQUESTED_WITH', 'xmlhttprequest');
      }
  });*/
});
</script>
</html>
