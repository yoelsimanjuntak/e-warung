<style>
#map {
    height: 400px;  /* The height is 400 pixels */
    width: 100%;  /* The width is the width of the web page */
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=site_url('admin/dashboard')?>"><i class="fa fa-home"></i> Home</a></li>
            <li class="breadcrumb-item active"><?= $title ?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-condensed nowrap">
                <thead>
                  <tr>
                    <th>Nama</th>
                    <th>Pemilik</th>
                    <th>Keterangan</th>
                    <th>Koordinat</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-koordinat" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Koordinat</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <div id="map"></div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var map; //Will contain map object.
var marker = false; ////Has the user plotted their location marker?

//Function called to initialize / create the map.
//This is called when the page has loaded.
function initMap() {
  var currLat = false;
  var currLng = false;

  //The center location of our map.
  var centerOfMap = new google.maps.LatLng(2.2587974,98.7436999);
  if(currLat && currLng) {
    centerOfMap = new google.maps.LatLng(parseFloat(currLat), parseFloat(currLng));
  }

  //Map options.
  var options = {
    center: centerOfMap, //Set center.
    zoom: 14 //The zoom value.
  };

  //Create the map object.
  map = new google.maps.Map(document.getElementById('map'), options);

  if(currLat && currLng) {
    var currLatLng = {lat: parseFloat(currLat),lng: parseFloat(currLng)};
    var currLocation = new google.maps.Marker({
      position: currLatLng,
      map: map,
      title: 'Lokasi sekarang',
      icon: {
          url: "http://maps.google.com/mapfiles/ms/icons/red-dot.png"
      }
    });
  }
}

$(document).ready(function() {
  var dataTable = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('admin/business/load')?>",
      "type": 'POST'
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    "dom":"R<'row'<'col-sm-4'l><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [],
    "columns": [
      {"orderable": true},
      {"orderable": true},
      {"orderable": false},
      {"orderable": true}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-koordinat', $(row)).click(function() {
        var lat = $(this).data('lat');
        var long = $(this).data('long');
        var name = $(this).data('name');

        if(lat && long) {
          var clickedLocation = {lat: parseFloat(lat),lng: parseFloat(long)};
          if(marker === false){
            marker = new google.maps.Marker({
              position: clickedLocation,
              map: map,
              draggable: true
            });
          } else{
            marker.setPosition(clickedLocation);
          }
          map.setCenter(clickedLocation);
          map.setZoom(16);
        }
        $('.modal-title', $('#modal-koordinat')).html(name);
        $('#modal-koordinat').modal('show');
        return false;
      });
    }
  });
});
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDzvhEg4efwXcYvdMvRLDGUEau4vjh8klg&callback=initMap">
</script>
