<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Login | <?= $this->setting_web_name ?></title>
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?=base_url()?>assets/js/jquery.particleground.js"></script>
  <style>
  .adsbox{
      background: none repeat scroll 0 0 #f5f5f5;
      border-radius: 10px;
      box-shadow: 0 0 5px 1px rgba(50, 50, 50, 0.2);
      margin: 20px auto 0;
      padding: 15px;
      border: 1px solid #caced3;
      height: 145px;
  }
  html, body {
      width: 100%;
      height: 100%;
      overflow: hidden;
  }
  #particles {
      width: 100%;
      height: 100%;
      overflow: hidden;
  }
  #intro {
      position: absolute;
      left: 0;
      top: 50%;
      padding: 0 20px;
      width: 100%;
      #text-align: center;
  }
  </style>
  <link rel="icon" type="image/png" href=<?=MY_IMAGEURL.$this->setting_web_logo?>>
</head>
<style>
    .no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
      position: fixed;
      left: 0px;
      top: 0px;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background: url(<?=base_url()?>assets/preloader/images/<?=$this->setting_web_preloader?>) center no-repeat #fff;
    }
</style>
<script>
    $(window).load(function() {
        $(".se-pre-con").fadeOut("slow");
    });
</script>

<body class="hold-transition login-page">
<div class="se-pre-con"></div>
<div id="particles">
  <div id="intro">
    <div class="login-box">
      <div class="card" style="background: rgba(255, 255, 255, 0.90);">
        <div class="card-body login-card-body" style="background: none">
          <div class="login-logo">
            <img class="user-image" src="<?=MY_IMAGEURL.$this->setting_web_logo?>" style="width: 60px" alt="Logo"><br  />
            <a href="<?=site_url()?>">
              <small class="text-muted font-weight-light"><?=$this->setting_web_name?></small>
              <p style="font-size: 10pt;"><?=$this->setting_web_desc?></p>
            </a>
          </div>
          <?php if(!empty($err)){ ?>
            <div class="callout callout-danger">
              <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;<?=$err?></span>
            </div>
          <?php } ?>

          <?= form_open(current_url(),array('id'=>'validate')) ?>
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="<?=COL_USERID?>" placeholder="Username" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="password" class="form-control" name="<?=COL_PASSWORD?>" placeholder="Password" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-key"></span>
              </div>
            </div>
          </div>

          <div class="footer" style="text-align: right;">
            <a href="<?=site_url('site/home')?>" class="btn btn-default"><i class="fa fa-home"></i>&nbsp;Home</a>
            <button type="submit" class="btn btn-success"><i class="fa fa-sign-in"></i>&nbsp;Masuk</button>
          </div>
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.validate.min.js"></script>
<script>
    $(document).ready(function() {
        $('#particles').particleground({
            dotColor: '#0074D9',
            lineColor: '#3D9970'
        });
        $('#intro').css({
            'margin-top': -($('#intro').height() / 2)
        });
    });
</script>
</body>
</html>
