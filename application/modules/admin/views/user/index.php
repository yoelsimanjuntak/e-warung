<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=site_url('admin/dashboard')?>"><i class="fa fa-home"></i> Home</a></li>
            <li class="breadcrumb-item active"><?= $title ?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">

        <div class="card card-default">
          <div class="card-header">
            <p class="mb-0">
              <?=anchor('admin/user/delete','<i class="fas fa-trash"></i> Hapus',array('class'=>'cekboxaction btn btn-danger btn-xs','data-confirm'=>'Apakah anda yakin?'))?>
              <?=anchor('admin/user/activate/0','<i class="fas fa-check"></i> Aktifkan',array('class'=>'cekboxaction btn btn-success btn-xs','data-confirm'=>'Apakah anda yakin?'))?>
              <?=anchor('admin/user/activate/1','<i class="fas fa-warning"></i> Suspend',array('class'=>'cekboxaction btn btn-secondary btn-xs','data-confirm'=>'Apakah anda yakin?'))?>
              <?=anchor('admin/user/add'.(!empty($role)?'/'.$role:''),'<i class="fas fa-plus"></i> Data Baru',array('class'=>'btn btn-primary btn-xs'))?>
            </p>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-condensed nowrap">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>Status</th>
                    <th>Email</th>
                    <th>NIK</th>
                    <th>Tgl. Bergabung</th>
                    <th>Login Terakhir</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {
  var dataTable = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('admin/user/load')?>",
      "type": 'POST',
      "data": {
        "role": <?=$role?>
      }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    "dom":"R<'row'<'col-sm-4'l><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [],
    "columnDefs":[
      {targets: [5,6], className:'dt-body-right'}
    ],
    "columns": [
      {"orderable": false,"width": "10px"},
      {"orderable": true},
      {"orderable": false,"width": "10px"},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true}
    ],
  });
  $('#cekbox').click(function() {
    if($(this).is(':checked')) {
      $('.cekbox').prop('checked',true);
    } else {
      $('.cekbox').prop('checked',false);
    }
  });
});
</script>
