-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 30, 2021 at 10:01 AM
-- Server version: 10.0.38-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 7.3.22-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `team1_ewarung`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_driver`
--

CREATE TABLE `m_driver` (
  `IdDriver` bigint(10) UNSIGNED NOT NULL,
  `IdPengguna` varchar(50) DEFAULT NULL,
  `Nama` varchar(50) NOT NULL DEFAULT '',
  `NoSIM` varchar(50) DEFAULT NULL,
  `NoPlat` varchar(50) DEFAULT NULL,
  `IsAktif` tinyint(1) DEFAULT NULL,
  `CreatedOn` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_driver`
--

INSERT INTO `m_driver` (`IdDriver`, `IdPengguna`, `Nama`, `NoSIM`, `NoPlat`, `IsAktif`, `CreatedOn`, `CreatedBy`) VALUES
(4, '32', 'Efren', '121234330', 'BB 1633 EE', 1, '2020-07-06 12:18:06', 'admin'),
(5, '33', 'Ishak', '13423433', 'BB 1564 EE', 1, '2020-07-06 12:19:02', 'admin'),
(6, '34', 'Juliana', '121221433', 'BB 1090 EE', 1, '2020-07-06 12:20:49', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `m_hari`
--

CREATE TABLE `m_hari` (
  `IdHari` bigint(10) UNSIGNED NOT NULL,
  `Hari` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_hari`
--

INSERT INTO `m_hari` (`IdHari`, `Hari`) VALUES
(1, 'Senin'),
(2, 'Selasa'),
(3, 'Rabu'),
(4, 'Kamis'),
(5, 'Jumat'),
(6, 'Sabtu'),
(8, 'Minggu');

-- --------------------------------------------------------

--
-- Table structure for table `m_resto`
--

CREATE TABLE `m_resto` (
  `IdResto` bigint(10) UNSIGNED NOT NULL,
  `IdPemilik` int(11) UNSIGNED NOT NULL,
  `Nama` varchar(50) NOT NULL DEFAULT '',
  `Alamat` text NOT NULL,
  `Lat` double NOT NULL,
  `Long` double NOT NULL,
  `IsHalal` tinyint(1) DEFAULT NULL,
  `IsBuka` tinyint(1) DEFAULT NULL,
  `IsAktif` tinyint(1) DEFAULT NULL,
  `CreatedOn` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_resto`
--

INSERT INTO `m_resto` (`IdResto`, `IdPemilik`, `Nama`, `Alamat`, `Lat`, `Long`, `IsHalal`, `IsBuka`, `IsAktif`, `CreatedOn`, `CreatedBy`) VALUES
(1, 6, 'Abc', 'Jl.Pahlawan', 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', ''),
(2, 14, 'RM.Padang', 'Jl.Sukarno', 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', ''),
(3, 15, 'KFC', 'Jl.Sm Raja', 2.735983600317561, 98.34527123719454, 0, 0, 0, '0000-00-00 00:00:00', ''),
(4, 21, '', '', 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', ''),
(5, 22, 'warung xxxx', 'fhjff\nhdjd\nbdbd', 2.7356898, 98.34525429999997, 0, NULL, NULL, '0000-00-00 00:00:00', ''),
(6, 24, '', '', 2.7358851414941463, 98.345219604671, NULL, NULL, NULL, '0000-00-00 00:00:00', ''),
(7, 25, '', '', 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', ''),
(8, 26, 'RM.Garuda', 'jl.singa', 1.0760654794092404, 101.22610084712505, NULL, NULL, NULL, '0000-00-00 00:00:00', ''),
(9, 27, '', '', 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', ''),
(10, 28, '', '', 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', ''),
(11, 29, 'Tondong Ta', 'jl letkol', 2.2625949660005125, 98.75828716903925, 1, 1, 1, '0000-00-00 00:00:00', ''),
(12, 35, 'Warung C', 'desa pasaribu dolok', 2.2620733479572777, 98.77215787768364, NULL, NULL, NULL, '0000-00-00 00:00:00', ''),
(13, 36, 'Warung boneka', 'desa pasaribu', 2.2637008492338992, 98.75982742756605, NULL, NULL, NULL, '0000-00-00 00:00:00', ''),
(14, 37, '', '', 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', ''),
(15, 38, 'Laperrrr', '', 2.261958437855748, 98.75719182193279, NULL, NULL, NULL, '0000-00-00 00:00:00', ''),
(16, 39, '', '', 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', ''),
(17, 44, 'Tondongta', 'jl Letkol manullang', 0, 0, 1, NULL, NULL, '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `m_restojadwal`
--

CREATE TABLE `m_restojadwal` (
  `IdJadwal` bigint(10) UNSIGNED NOT NULL,
  `IdResto` bigint(10) UNSIGNED NOT NULL,
  `IdHari` bigint(10) UNSIGNED NOT NULL,
  `JamBuka` varchar(10) NOT NULL DEFAULT '',
  `JamTutup` varchar(10) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_restojadwal`
--

INSERT INTO `m_restojadwal` (`IdJadwal`, `IdResto`, `IdHari`, `JamBuka`, `JamTutup`) VALUES
(1, 3, 2, '07:00', '07:00'),
(2, 3, 1, '07:00', '07:00'),
(3, 11, 1, '07:00', '07:00'),
(4, 11, 2, '07:00', '07:00');

-- --------------------------------------------------------

--
-- Table structure for table `m_restomenu`
--

CREATE TABLE `m_restomenu` (
  `IdMenu` bigint(10) UNSIGNED NOT NULL,
  `IdResto` bigint(10) UNSIGNED NOT NULL,
  `Menu` varchar(50) NOT NULL,
  `Deskripsi` text NOT NULL,
  `Harga` double NOT NULL,
  `IsTersedia` tinyint(1) NOT NULL,
  `IsAktif` tinyint(1) NOT NULL,
  `FileGambar` varchar(200) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_restomenu`
--

INSERT INTO `m_restomenu` (`IdMenu`, `IdResto`, `Menu`, `Deskripsi`, `Harga`, `IsTersedia`, `IsAktif`, `FileGambar`, `CreatedOn`, `CreatedBy`) VALUES
(1, 1, 'menu1', '50000', 50000, 0, 0, 'FileGambar1593012660529.jpg', '0000-00-00 00:00:00', ''),
(2, 1, 'menu2', 'mmmmm', 50000, 0, 0, 'FileGambar1593012314365.jpg', '0000-00-00 00:00:00', ''),
(3, 1, 'menu3', '10000', 10000, 0, 0, 'FileGambar1593012506576.jpg', '0000-00-00 00:00:00', ''),
(4, 1, 'jeje', 'whheje', 50000, 0, 0, 'FileGambar1593012747831.jpg', '0000-00-00 00:00:00', ''),
(5, 3, 'nama menu', 'keterangan\nket\nket', 50000, 1, 1, 'FileGambar1593351221151.jpg', '0000-00-00 00:00:00', ''),
(6, 3, 'nasi goreng', 'bla bla bla\nbla bla\nbla', 12000, 1, 1, 'FileGambar1593351660926.jpg', '0000-00-00 00:00:00', ''),
(7, 3, 'menu tiga', 'ooo\noo\no', 17000, 0, 1, 'FileGambar1593351082050.jpg', '0000-00-00 00:00:00', ''),
(8, 3, 'menu 4', 'ppppp\npppp\nppp', 30000, 1, 1, 'FileGambar1593351581112.jpg', '0000-00-00 00:00:00', ''),
(9, 3, 'ghu', 'hhuu', 15000, 1, 1, 'FileGambar1593355796100.jpg', '0000-00-00 00:00:00', ''),
(10, 3, 'menuuuuu', '', 20000, 1, 1, 'FileGambar1593396823027.jpg', '0000-00-00 00:00:00', ''),
(11, 3, 'ndjjdd', 'ejrjrj\nffrjjr\nff\nbdnd', 500000, 1, 1, 'FileGambar1593595879496.jpg', '0000-00-00 00:00:00', ''),
(12, 3, '', '', 0, 0, 0, '', '0000-00-00 00:00:00', ''),
(13, 3, '', '', 0, 0, 0, '', '0000-00-00 00:00:00', ''),
(14, 3, '', '', 0, 0, 0, '', '0000-00-00 00:00:00', ''),
(15, 3, '', '', 0, 0, 0, '', '0000-00-00 00:00:00', ''),
(16, 3, '', '', 0, 0, 0, '', '0000-00-00 00:00:00', ''),
(17, 3, '', '', 0, 0, 0, '', '0000-00-00 00:00:00', ''),
(18, 3, '', '', 0, 0, 0, '', '0000-00-00 00:00:00', ''),
(19, 3, '', '', 0, 0, 0, '', '0000-00-00 00:00:00', ''),
(20, 3, 'menu baruuu', 'kettyy', 25000, 1, 1, 'FileGambar1593828033407.jpg', '0000-00-00 00:00:00', ''),
(21, 5, 'menuuuuuu', 'dhdjd\nhdhdj\ngdhd', 25000, 1, 1, 'FileGambar1618321349844.jpg', '0000-00-00 00:00:00', ''),
(22, 5, 'memmmmm', 'bdnnd', 25000, 1, 1, 'FileGambar1618412254502.jpg', '0000-00-00 00:00:00', ''),
(23, 5, 'jjnj', 'jnj ', 50000, 1, 0, '', '0000-00-00 00:00:00', ''),
(24, 6, 'nasi ayam goreng', 'jsnd\ndnnd\ndhdh', 30000, 1, 1, 'FileGambar1593929766205.jpg', '0000-00-00 00:00:00', ''),
(25, 8, 'Ayam rica rica', 'enak', 19900, 1, 1, 'FileGambar1593933354359.jpg', '0000-00-00 00:00:00', ''),
(26, 8, 'es teh', 'lezat', 50000, 1, 1, 'FileGambar1593933418352.jpg', '0000-00-00 00:00:00', ''),
(27, 8, 'ayam bakar', '', 21000, 1, 1, 'FileGambar1593933514587.jpg', '0000-00-00 00:00:00', ''),
(28, 11, 'nasi ayam kampung', 'nasi dengan ayam goreng', 27000, 1, 1, 'FileGambar1594000786189.jpg', '0000-00-00 00:00:00', ''),
(29, 11, 'nasi Goreng', 'nasi goreng', 20000, 1, 1, 'FileGambar1594000820866.jpg', '0000-00-00 00:00:00', ''),
(30, 11, 'ikan bakar', 'ikan', 25000, 1, 1, 'FileGambar1594001296983.jpg', '0000-00-00 00:00:00', ''),
(31, 11, 'ayam sambal', 'ayam kampung goreng dengan sambal', 35000, 1, 1, 'FileGambar1594002120994.jpg', '0000-00-00 00:00:00', ''),
(32, 11, 'Nasi Goreng', 'Nasi Goreng Spesial', 25000, 1, 1, 'FileGambar1594191626654.jpg', '0000-00-00 00:00:00', ''),
(33, 15, 'Ayam Bakar Goreng', 'Ayam bakar semalam, masih sehat jadi di panasi dengan cara di goreng, murah dan enak, di jamin kok', 12000, 1, 1, 'FileGambar1594191930615.jpg', '0000-00-00 00:00:00', ''),
(34, 11, 'nama menu', 'penjelasan', 15000, 1, 1, 'FileGambar1597813117668.jpg', '0000-00-00 00:00:00', ''),
(35, 17, 'Ikan Jahir Asam Manis', 'Ikan Jahir goreng dengan bumbu asam manis', 30000, 1, 1, 'FileGambar1605754072156.jpg', '0000-00-00 00:00:00', ''),
(36, 17, 'Ayam Goreng', 'Nasi dengan Ayam Goreng', 22000, 1, 1, 'FileGambar1605754131603.jpg', '0000-00-00 00:00:00', ''),
(37, 17, 'Mie Hot Plate Seafood', 'Mi dengan Penyajian Hot Plate', 23000, 1, 1, 'FileGambar1605754189742.jpg', '0000-00-00 00:00:00', ''),
(38, 17, 'Jus Jeruk', 'Jus Jeruk Segar', 13000, 1, 1, 'FileGambar1605754232694.jpg', '0000-00-00 00:00:00', ''),
(39, 17, 'Jus Nenas', 'Jus Nenas Segar', 12000, 1, 1, 'FileGambar1605754254279.jpg', '0000-00-00 00:00:00', ''),
(40, 17, 'Udang Asam Manis', 'udang dengan saos asam manis', 35000, 1, 1, 'FileGambar1605754282851.jpg', '0000-00-00 00:00:00', ''),
(41, 17, 'Soul Ikan Mujahir', 'ikan Mujahir dengan SOP enak', 25000, 1, 1, 'FileGambar1605754326883.jpg', '0000-00-00 00:00:00', ''),
(42, 17, 'Nasi Goreng', 'Nasi goreng Spesial', 27000, 1, 1, 'FileGambar1605754408153.jpg', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `ongkir`
--

CREATE TABLE `ongkir` (
  `harga_ongkir` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ongkir`
--

INSERT INTO `ongkir` (`harga_ongkir`) VALUES
(10000);

-- --------------------------------------------------------

--
-- Table structure for table `t_pesanan`
--

CREATE TABLE `t_pesanan` (
  `IdPesanan` bigint(10) UNSIGNED NOT NULL,
  `IdResto` bigint(10) UNSIGNED NOT NULL,
  `IdDriver` bigint(10) UNSIGNED NOT NULL,
  `IdPembeli` varchar(50) NOT NULL DEFAULT '',
  `Status` varchar(50) NOT NULL DEFAULT '' COMMENT 'MENUNGGU, DITOLAK, DITERIMA, DIKIRIM, SELESAI',
  `BiayaKirim` double NOT NULL COMMENT 'Ongkir',
  `BiayaTotal` double NOT NULL COMMENT 'Ongkir + Subtotal (menu)',
  `Tujuan` text NOT NULL,
  `Lat` double NOT NULL,
  `Long` double NOT NULL,
  `Jarak` double NOT NULL COMMENT 'Jarak warung ke tujuan',
  `Foto` varchar(200) DEFAULT NULL,
  `Foto2` varchar(200) DEFAULT NULL,
  `RatingResto` double DEFAULT NULL COMMENT 'Bintang utk warung',
  `RatingDriver` double DEFAULT NULL COMMENT 'Bintang utk driver',
  `Feedback` text,
  `AlasanTolak` text,
  `CreatedOn` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pesanan`
--

INSERT INTO `t_pesanan` (`IdPesanan`, `IdResto`, `IdDriver`, `IdPembeli`, `Status`, `BiayaKirim`, `BiayaTotal`, `Tujuan`, `Lat`, `Long`, `Jarak`, `Foto`, `Foto2`, `RatingResto`, `RatingDriver`, `Feedback`, `AlasanTolak`, `CreatedOn`, `CreatedBy`) VALUES
(1, 11, 32, '29', 'SELESAI', 10000, 60000, 'Kominfo humbang', 2.2587325, 98.7498711, 1, 'FileGambar1597813284488.jpg', NULL, NULL, NULL, NULL, NULL, '2020-08-19 12:00:35', ''),
(2, 17, 0, '45', 'DITOLAK', 109942240, 110066240, 'Dinas Komunikasi dan informasi Humbang Hasundutan', 2.2658785, 98.7699017, 10994224, NULL, NULL, NULL, NULL, NULL, 'salah pesan', '2020-11-19 10:01:11', ''),
(3, 17, 0, '45', 'DITOLAK', 109942240, 110066240, 'Dinas Komunikasi dan informasi Humbang Hasundutan', 2.2658785, 98.7699017, 10994224, NULL, NULL, NULL, NULL, NULL, 'salah pesan', '2020-11-19 10:01:14', ''),
(4, 17, 0, '45', 'DITOLAK', 109942240, 110066240, 'Dinas Komunikasi dan informasi Humbang Hasundutan', 2.2658785, 98.7699017, 10994224, NULL, NULL, NULL, NULL, NULL, 'tolak pesan', '2020-11-19 10:01:14', ''),
(5, 17, 0, '45', 'SELESAI', 109942240, 110066240, 'Dinas Komunikasi dan informasi Humbang Hasundutan', 2.2658785, 98.7699017, 10994224, 'FileGambar1605755189839.jpg', NULL, NULL, NULL, NULL, NULL, '2020-11-19 10:01:18', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_pesananlog`
--

CREATE TABLE `t_pesananlog` (
  `IdLog` bigint(10) UNSIGNED NOT NULL,
  `IdPesanan` bigint(10) UNSIGNED NOT NULL,
  `Status` varchar(50) NOT NULL DEFAULT '',
  `Deskripsi` text,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_pesananmenu`
--

CREATE TABLE `t_pesananmenu` (
  `Uniq` bigint(10) UNSIGNED NOT NULL,
  `IdPesanan` bigint(10) UNSIGNED NOT NULL,
  `IdMenu` bigint(10) UNSIGNED NOT NULL,
  `Harga` double NOT NULL,
  `Jumlah` double NOT NULL,
  `Subtotal` double NOT NULL,
  `Catatan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pesananmenu`
--

INSERT INTO `t_pesananmenu` (`Uniq`, `IdPesanan`, `IdMenu`, `Harga`, `Jumlah`, `Subtotal`, `Catatan`) VALUES
(1, 1, 32, 25000, 1, 25000, ''),
(2, 1, 30, 25000, 1, 25000, ''),
(3, 2, 42, 27000, 1, 27000, ''),
(4, 3, 42, 27000, 1, 27000, ''),
(5, 4, 42, 27000, 1, 27000, ''),
(6, 2, 41, 25000, 1, 25000, ''),
(7, 3, 41, 25000, 1, 25000, ''),
(8, 2, 39, 12000, 1, 12000, ''),
(9, 4, 41, 25000, 1, 25000, ''),
(10, 3, 39, 12000, 1, 12000, ''),
(11, 2, 35, 30000, 2, 60000, ''),
(12, 4, 39, 12000, 1, 12000, ''),
(13, 3, 35, 30000, 2, 60000, ''),
(14, 4, 35, 30000, 2, 60000, ''),
(15, 5, 42, 27000, 1, 27000, ''),
(16, 5, 41, 25000, 1, 25000, ''),
(17, 5, 39, 12000, 1, 12000, ''),
(18, 5, 35, 30000, 2, 60000, '');

-- --------------------------------------------------------

--
-- Table structure for table `_roles`
--

CREATE TABLE `_roles` (
  `RoleID` bigint(10) UNSIGNED NOT NULL,
  `RoleName` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_settings`
--

CREATE TABLE `_settings` (
  `SettingID` int(10) UNSIGNED NOT NULL,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_settings`
--

INSERT INTO `_settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`) VALUES
(1, 'SETTING_WEB_NAME', 'SETTING_WEB_NAME', 'E-WARUNG\n'),
(2, 'SETTING_WEB_DESC', 'SETTING_WEB_DESC', 'Layanan Pesan Antar Kabupaten Humbang Hasundutan'),
(3, 'SETTING_WEB_DISQUS_URL', 'SETTING_WEB_DISQUS_URL', '-'),
(4, 'SETTING_ORG_NAME', 'SETTING_ORG_NAME', 'Pemerintah Kabupaten Humbang Hasundutan'),
(5, 'SETTING_ORG_ADDRESS', 'SETTING_ORG_ADDRESS', '-'),
(6, 'SETTING_ORG_LAT', 'SETTING_ORG_LAT', ''),
(7, 'SETTING_ORG_LONG', 'SETTING_ORG_LONG', ''),
(8, 'SETTING_ORG_PHONE', 'SETTING_ORG_PHONE', '-'),
(9, 'SETTING_ORG_FAX', 'SETTING_ORG_FAX', '-'),
(10, 'SETTING_ORG_MAIL', 'SETTING_ORG_MAIL', '-'),
(11, 'SETTING_WEB_API_FOOTERLINK', 'SETTING_WEB_API_FOOTERLINK', '-'),
(12, 'SETTING_WEB_LOGO', 'SETTING_WEB_LOGO', 'logo.png'),
(13, 'SETTING_WEB_SKIN_CLASS', 'SETTING_WEB_SKIN_CLASS', 'skin-green-light'),
(14, 'SETTING_WEB_PRELOADER', 'SETTING_WEB_PRELOADER', 'loader-128x/Preloader_2.gif'),
(15, 'SETTING_WEB_VERSION', 'SETTING_WEB_VERSION', '1.0');

-- --------------------------------------------------------

--
-- Table structure for table `_users`
--

CREATE TABLE `_users` (
  `UserID` int(11) UNSIGNED NOT NULL,
  `Password` varchar(200) NOT NULL DEFAULT '',
  `RoleID` bigint(10) NOT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) DEFAULT NULL,
  `Email` varchar(200) NOT NULL DEFAULT '',
  `Nama` varchar(200) NOT NULL DEFAULT '',
  `NIK` varchar(200) DEFAULT NULL,
  `NoTelp` varchar(200) DEFAULT NULL,
  `Alamat` text,
  `TanggalLahir` date DEFAULT NULL,
  `Foto` text,
  `FotoKTP` text,
  `FotoKTP2` text,
  `TanggalRegistrasi` date DEFAULT NULL,
  `IsEmailVerified` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_users`
--

INSERT INTO `_users` (`UserID`, `Password`, `RoleID`, `LastLogin`, `IsSuspend`, `Email`, `Nama`, `NIK`, `NoTelp`, `Alamat`, `TanggalLahir`, `Foto`, `FotoKTP`, `FotoKTP2`, `TanggalRegistrasi`, `IsEmailVerified`) VALUES
(12, 'e10adc3949ba59abbe56e057f20f883e', 3, NULL, NULL, 'rolassimanjuntak@gmail.com', 'Rolas', '1271031208950002', '085359867032', 'Medan', '1995-08-12', 'foto-1271031208950002-20200622163811.png', 'ktp-1271031208950002-20200622163811.png', NULL, '2020-06-22', NULL),
(13, '354259334e3e5522120d50bad3bf44cd', 1, NULL, NULL, 'admin', 'Administrator', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'e10adc3949ba59abbe56e057f20f883e', 3, NULL, NULL, 'sam@gmail', 'sam', '00888811111', '082277109994', 'fgh bbjjbbaaaa', NULL, 'foto_member/Foto_082277109994_202006292004.jpg', 'foto_member/FotoKTP_082277109994_202006292004.jpg', 'foto_member/FotoKTP2_082277109994_202006292004.jpg', '2020-06-29', NULL),
(22, 'e10adc3949ba59abbe56e057f20f883e', 2, NULL, NULL, 'berkat@gmail.com', 'b junaidi banurea', '74737373', '082163737711', 'alamt\nalamttt', NULL, NULL, NULL, NULL, '2020-07-04', NULL),
(23, 'e10adc3949ba59abbe56e057f20f883e', 3, NULL, NULL, 'berkat.junaidi.b@gmail.com', 'berkat j', '158404864684', '082163737711', 'sdk', NULL, 'foto_member/Foto_082163737711_202007051208.jpg', 'foto_member/FotoKTP_082163737711_202007051208.jpg', 'foto_member/FotoKTP2_082163737711_202007051208.jpg', '2020-07-05', NULL),
(44, 'e10adc3949ba59abbe56e057f20f883e', 2, NULL, NULL, 'rirismanik17@gmail.com', 'Riris', '1212075711930001', '082370719569', 'Jl Letkol Manullang', NULL, NULL, NULL, NULL, '2020-11-19', NULL),
(45, 'e10adc3949ba59abbe56e057f20f883e', 3, NULL, NULL, 'aekriman123@gmail.com', 'Rianti Zega', '1212075844630001', '08126468377', 'Kominfo Humbang Hasundutan', NULL, 'foto_member/Foto_08126468377_202011190957.jpg', 'foto_member/FotoKTP_08126468377_202011190957.jpg', 'foto_member/FotoKTP2_08126468377_202011190957.jpg', '2020-11-19', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_driver`
--
ALTER TABLE `m_driver`
  ADD PRIMARY KEY (`IdDriver`);

--
-- Indexes for table `m_hari`
--
ALTER TABLE `m_hari`
  ADD PRIMARY KEY (`IdHari`);

--
-- Indexes for table `m_resto`
--
ALTER TABLE `m_resto`
  ADD PRIMARY KEY (`IdResto`),
  ADD KEY `IdPemilik` (`IdPemilik`);

--
-- Indexes for table `m_restojadwal`
--
ALTER TABLE `m_restojadwal`
  ADD PRIMARY KEY (`IdJadwal`),
  ADD KEY `FK_RestoJadwal_Resto` (`IdResto`),
  ADD KEY `FK_RestoJadwal_Hari` (`IdHari`);

--
-- Indexes for table `m_restomenu`
--
ALTER TABLE `m_restomenu`
  ADD PRIMARY KEY (`IdMenu`),
  ADD KEY `FK_RestoMenu_Resto` (`IdResto`);

--
-- Indexes for table `t_pesanan`
--
ALTER TABLE `t_pesanan`
  ADD PRIMARY KEY (`IdPesanan`),
  ADD KEY `FK_Pesanan_Resto` (`IdResto`);

--
-- Indexes for table `t_pesananlog`
--
ALTER TABLE `t_pesananlog`
  ADD PRIMARY KEY (`IdLog`),
  ADD KEY `FK_PesananLog_Pesanan` (`IdPesanan`);

--
-- Indexes for table `t_pesananmenu`
--
ALTER TABLE `t_pesananmenu`
  ADD PRIMARY KEY (`Uniq`),
  ADD KEY `FK_PesananMenu_Pesanan` (`IdPesanan`),
  ADD KEY `FK_PesananMenu_Menu` (`IdMenu`);

--
-- Indexes for table `_roles`
--
ALTER TABLE `_roles`
  ADD PRIMARY KEY (`RoleID`);

--
-- Indexes for table `_settings`
--
ALTER TABLE `_settings`
  ADD PRIMARY KEY (`SettingID`);

--
-- Indexes for table `_users`
--
ALTER TABLE `_users`
  ADD PRIMARY KEY (`UserID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_driver`
--
ALTER TABLE `m_driver`
  MODIFY `IdDriver` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `m_hari`
--
ALTER TABLE `m_hari`
  MODIFY `IdHari` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `m_resto`
--
ALTER TABLE `m_resto`
  MODIFY `IdResto` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `m_restojadwal`
--
ALTER TABLE `m_restojadwal`
  MODIFY `IdJadwal` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `m_restomenu`
--
ALTER TABLE `m_restomenu`
  MODIFY `IdMenu` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `t_pesanan`
--
ALTER TABLE `t_pesanan`
  MODIFY `IdPesanan` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_pesananlog`
--
ALTER TABLE `t_pesananlog`
  MODIFY `IdLog` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_pesananmenu`
--
ALTER TABLE `t_pesananmenu`
  MODIFY `Uniq` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `_roles`
--
ALTER TABLE `_roles`
  MODIFY `RoleID` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `_settings`
--
ALTER TABLE `_settings`
  MODIFY `SettingID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `_users`
--
ALTER TABLE `_users`
  MODIFY `UserID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
